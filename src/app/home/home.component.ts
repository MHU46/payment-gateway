import { Component, OnInit } from '@angular/core';
import { InstamojoService } from '../models/instamojo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private _instaServ: InstamojoService
  ) { }

  id:string= 'MOJO7b22005N48642099';

  ngOnInit() {
    console.log('in Home component');
    this._instaServ.getPaymentDetails(this.id).subscribe(res => {
      console.log('get payments details req called in component, res = ', res);
    }); 
  }
}
