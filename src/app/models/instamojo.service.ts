import {
    Injectable,
    Inject,
    EventEmitter
} from '@angular/core';
  import {
    Http,
    Headers,
    RequestOptions
} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
  
@Injectable()
export class InstamojoService {

constructor(
    private _http: Http
) { }

    /**
     * to create payment request, this will generate the payment ID.
     */
    public createRequest() {
        const headers = {
        'X-Api-Key': 'test_86115178c67653ce90c174b5eab',
        'X-Auth-Token': 'test_b39483ce7c43849410d81ef83ac'
        };
        const url = 'https://www.instamojo.com/api/1.1/payment-requests/';
        const payload = {
        purpose: 'FIFA 16',
        amount: '2500',
        phone: '9999999999',
        buyer_name: 'John Doe',
        redirect_url: 'http://localhost:4200',
        send_email: true,
        webhook: 'http://www.example.com/webhook/',
        send_sms: true,
        email: 'foo@example.com',
        allow_repeated_payments: false
        };
        const options = new RequestOptions({
        headers: new Headers(headers)
        });
        return this._http.post(url, payload, options).map((res) => {
        console.log('request created, res = ', res);
        }, err => {
        console.log(err);
        });
    }

    /**
     * get call to fetch all payment requests created so far.
    */
    getPaymentRequests() {
        const url = 'https://www.instamojo.com/api/1.1/payment-requests/';
        const headers = {
        'X-Api-Key': 'd82016f839e13cd0a79afc0ef5b288b3',
        'X-Auth-Token': '3827881f669c11e8dad8a023fd1108c2'
        };
        const options = new RequestOptions({
        headers: new Headers(headers)
        });
        return this._http.get(url, options)
        .map(resp => {
        return resp;
        }, err => {
        console.log(err);
        });
    }

    /**
     * get call to fetch details of a particular payment request with a given ID.
     * @param id
     */
    getPaymentRequestsById(id) {
        const url = 'https://www.instamojo.com/api/1.1/payment-requests/' + id;
        const headers = {
        'X-Api-Key': 'd82016f839e13cd0a79afc0ef5b288b3',
        'X-Auth-Token': '3827881f669c11e8dad8a023fd1108c2'
        };
        const options = new RequestOptions({
        headers: new Headers(headers)
        });
        return this._http.get(url, options)
        .map(resp => {
        return resp;
        }, err => {
        console.log(err);
        });
    }

    /**
     * To create Refund request.
     */
    public createRefund() {
        const headers = {
        'X-Api-Key': 'd82016f839e13cd0a79afc0ef5b288b3',
        'X-Auth-Token': '3827881f669c11e8dad8a023fd1108c2'
        };
        const url = 'https://www.instamojo.com/api/1.1/refunds/';
        const payload = {
            payment_id: "MOJO5a06005J21512197",
            type: "QFL",
            body: "Customer isn't satisfied with the quality"
        };
        const options = new RequestOptions({
        headers: new Headers(headers)
        });
        return this._http.post(url, payload, options).map((res) => {
        console.log('request created, res = ', res);
        }, err => {
        console.log(err);
        });
    }

    /**
     * get call to fetch all refunds requests created so far.
    */
    getRefundRequests() {
        const url = 'https://www.instamojo.com/api/1.1/refunds/';
        const headers = {
        'X-Api-Key': 'd82016f839e13cd0a79afc0ef5b288b3',
        'X-Auth-Token': '3827881f669c11e8dad8a023fd1108c2'
        };
        const options = new RequestOptions({
        headers: new Headers(headers)
        });
        return this._http.get(url, options)
        .map(resp => {
        return resp;
        }, err => {
        console.log(err);
        });
    }

    /**
     * get call to fetch details fo the payments with a given ID.
     * @param id
     */
    getPaymentDetails(MOJO7b22005N48642099) {
        const url = 'https://www.instamojo.com/api/1.1/payments/' + MOJO7b22005N48642099;
        const headers = {
        'X-Api-Key': 'test_86115178c67653ce90c174b5eab',
        'X-Auth-Token': 'test_b39483ce7c43849410d81ef83ac'
        };
        const options = new RequestOptions({
        headers: new Headers(headers)
        });
        return this._http.get(url, options)
        .map(resp => {
        return resp;
        }, err => {
        console.log(err);
        });
    }
}
  